<?php 

    $args = [
        'post_type' => 'payment_methods',
    ];


        $query = new WP_Query($args);

?>


    <?php if( $query->have_posts() ):?>

        <?php while ( $query->have_posts() ) : $query->the_post();?>

    <div class ="all-payments-list">
        <div class ="all-payments">
            <div class ="all-payments__logo">
                <?php if( get_field('payment_logo') ): ?>
                    <img src="<?php the_field('payment_logo'); ?>" />
                <?php endif; ?>
            </div>

            <div class ="all-payments__title">
                <?php the_field('payment_title'); ?>
            </div>

            <div class ="all-payments__description">
                <?php the_field('payment_description'); ?>
            </div>

            <div class ="all-payments__readmore">
                <?php 
                    $read = get_field('payment_readmore');
                    if( $read ): ?>
                    <div class = "read-btn"><a href="<?php echo esc_url( $read ); ?>">Read More</a></div>
                <?php endif; ?>
             </div>
        </div>
    </div>

        <?php endwhile; // end of the loop. ?>
        
<?php endif;?>